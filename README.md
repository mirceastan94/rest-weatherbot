WeatherBOT, developed in 2018, using Java/Spring Boot, with the help of the official Telegram Java API, is currently still more complex than any other chatbot on this subject, offering unprecedented weather related features, such as:

• current weather information for a desired location, either by:

- sharing the current location of one’s smartphone;

- specifying a dedicated location manually, anywhere around the globe.

• today’s and next 3 days forecast information for the searched location;

• received daily notifications with the current weather for a specified location, by registering in the database;

• location(s) personal search history.