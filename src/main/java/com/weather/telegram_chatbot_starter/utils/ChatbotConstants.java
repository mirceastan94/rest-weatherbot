package com.weather.telegram_chatbot_starter.utils;

public class ChatbotConstants {

	public final static String START = "/start";
	public final static String DENY_EN = "Deny";
	public final static String FAVORITE_LOCATION_EN = "/fav ";
	public final static String CHOSEN_LOCATION_CURRENT_WEATHER_EN = "/loc ";
	public final static String NOTIFICATION_HOUR_EN = "/hour ";
	public final static String SHOW_FORECAST_EN = "Show forecast";
	public final static String ANOTHER_LOCATION_WEATHER_INFO_EN = "Another location weather information";
	public final static String SEARCH_USER_LIST_HISTORY_EN = "Personal search history";
	public final static String BACK_TO_MENU_EN = "Go back to menu";
	public final static String APPLICATION_SETTINGS_EN = "Settings";
	public final static String NOTIFICATIONS_ON_EN = "Turn weather notifications on";
	public final static String NOTIFICATIONS_OFF_EN = "Turn weather notifications off";
	public final static String UPDATE_LOCATION_INFORMATION_EN = "Update location information";
	public final static String CHANGE_LANGUAGE_EN = "Change language";
	public final static String LANGUAGE_ENGLISH_EN = "English";
	public final static String DELETE_PROFILE_EN = "Delete profile";
	public final static String SHARED_LOCATION_CURRENT_WEATHER_EN = "Current location weather information";
	public final static String SHARED_CONTACT_DETAILS_EN = "Share contact details";

	public final static String DENY_DE = "Verweigern";
	public final static String FAVORITE_LOCATION_DE = "/fav ";
	public final static String CHOSEN_LOCATION_CURRENT_WEATHER_DE = "/loc ";
	public final static String NOTIFICATION_HOUR_DE = "/hour ";
	public final static String SHOW_FORECAST_DE = "Prognose anzeigen";
	public final static String ANOTHER_LOCATION_WEATHER_INFO_DE = "Ein anderer Standort Wetterinformationen";
	public final static String SEARCH_USER_LIST_HISTORY_DE = "Pers�nliche Suchhistorie";
	public final static String BACK_TO_MENU_DE = "Gehe zur�ck zum Men�";
	public final static String APPLICATION_SETTINGS_DE = "Einstellungen";
	public final static String NOTIFICATIONS_ON_DE = "Schalten Sie die Wettermeldungen ein";
	public final static String NOTIFICATIONS_OFF_DE = "Schalten Sie die Wettermeldungen aus";
	public final static String UPDATE_LOCATION_INFORMATION_DE = "Aktualisieren Sie die Standortinformationen";
	public final static String CHANGE_LANGUAGE_DE = "Sprache �ndern";
	public final static String LANGUAGE_GERMAN_DE = "Deutsch";
	public final static String DELETE_PROFILE_DE = "Profil l�schen";
	public final static String SHARED_CONTACT_DETAILS_DE = "Kontaktdaten teilen";

	public static final String LOC = "/loc ";
	public static final String TEST = "test";
	public static final String LOC_N_A = "/loc N/A";
	public static final String N_A = "N/A";

}
