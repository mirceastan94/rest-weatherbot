package com.weather.telegram_chatbot_starter.utils;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.model.request.KeyboardButton;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.weather.telegram_chatbot_starter.config.Properties;
import com.weather.telegram_chatbot_starter.model.Location;
import com.weather.telegram_chatbot_starter.model.Person;
import com.weather.telegram_chatbot_starter.repo.PersonRepo;

@Component
public final class MenuUtils {

	@Autowired
	private Properties properties;

	@Autowired
	private PersonRepo personRepo;

	@Autowired
	private MessageSourceAccessor messageSourceAccessor;

	/**
	 * This method generates a menu with the core features of this WeatherBOT
	 * 
	 * @param userId
	 * @return
	 */
	public ReplyKeyboardMarkup showMainMenu(Integer userId) {

		final Locale currentUserLocale = getCurrentUserLocale(userId);

		final KeyboardButton currentLocationWeather = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getCurrentLocationWeatherInformation(), currentUserLocale));
		final KeyboardButton otherLocationWeather = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getAnotherLocationWeatherInformation(), currentUserLocale));
		final KeyboardButton userSearchHistory = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getSearchListHistory(), currentUserLocale));
		final KeyboardButton appSettings = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getApplicationSettings(), currentUserLocale));

		currentLocationWeather.requestLocation(true);

		final KeyboardButton[][] buttonsList = new KeyboardButton[2][2];
		buttonsList[0][0] = currentLocationWeather;
		buttonsList[0][1] = otherLocationWeather;
		buttonsList[1][0] = userSearchHistory;
		buttonsList[1][1] = appSettings;

		final ReplyKeyboardMarkup menuReplyKeyboard = new ReplyKeyboardMarkup(buttonsList);
		menuReplyKeyboard.resizeKeyboard(true);
		menuReplyKeyboard.oneTimeKeyboard(true);
		return menuReplyKeyboard;
	}

	/**
	 * This method generates a keyboard markup, that allows the user to whether
	 * check the forecast for the desired location or go back to the main menu
	 * 
	 * @param userId
	 * @return
	 */
	public ReplyKeyboardMarkup showForecastMenu(Integer userId) {

		final Locale currentUserLocale = getCurrentUserLocale(userId);

		final KeyboardButton showForecastButton = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getShowForecast(), currentUserLocale));
		final KeyboardButton goToMenuButton = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getGoBackToMenu(), currentUserLocale));

		final KeyboardButton[][] buttonsList = new KeyboardButton[1][2];
		buttonsList[0][0] = showForecastButton;
		buttonsList[0][1] = goToMenuButton;

		final ReplyKeyboardMarkup forecastReplyKeyboard = new ReplyKeyboardMarkup(buttonsList);
		forecastReplyKeyboard.resizeKeyboard(true);
		forecastReplyKeyboard.oneTimeKeyboard(true);
		return forecastReplyKeyboard;
	}

	/**
	 * This method exposes a two button keyboard for either sharing the contact
	 * details of the user or denying this process
	 * 
	 * @param userId
	 * @return
	 */
	public ReplyKeyboardMarkup shareDetailsMenu(Integer userId) {

		final Locale currentUserLocale = getCurrentUserLocale(userId);

		final KeyboardButton contactButton = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getShareContactDetails(), currentUserLocale));
		contactButton.requestContact(true);

		final KeyboardButton denyButton = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getDeny(), currentUserLocale));

		final KeyboardButton[][] buttonsList = new KeyboardButton[1][2];
		buttonsList[0][0] = contactButton;
		buttonsList[0][1] = denyButton;

		final ReplyKeyboardMarkup contactDetailsKeyboard = new ReplyKeyboardMarkup(buttonsList);
		contactDetailsKeyboard.resizeKeyboard(true);
		contactDetailsKeyboard.oneTimeKeyboard(true);

		return contactDetailsKeyboard;
	}

	/**
	 * This method generates a menu with the Settings exposed by this Bot
	 * 
	 * @param userId
	 * @return
	 */
	public ReplyKeyboardMarkup showSettingsMenu(Integer userId) {

		final Person person = personRepo.findById(userId);

		final Locale currentUserLocale = getCurrentUserLocale(userId);

		final KeyboardButton changeNotificationsState;

		if (person.getNotificationStatus().equals(Boolean.TRUE.toString())) {
			changeNotificationsState = new KeyboardButton(
					messageSourceAccessor.getMessage(properties.getNotificationsOff(), currentUserLocale));
		} else {
			changeNotificationsState = new KeyboardButton(
					messageSourceAccessor.getMessage(properties.getNotificationsOn(), currentUserLocale));
		}
		final KeyboardButton updateLocationInformation = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getUpdateLocationInformation(), currentUserLocale));
		updateLocationInformation.requestContact(true);

		final KeyboardButton changeLanguage = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getChangeLanguage(), currentUserLocale));

		final KeyboardButton deleteProfile = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getDeleteProfile(), currentUserLocale));

		final KeyboardButton[][] buttonsList = new KeyboardButton[2][2];
		buttonsList[0][0] = changeNotificationsState;
		buttonsList[0][1] = updateLocationInformation;
		buttonsList[1][0] = changeLanguage;
		buttonsList[1][1] = deleteProfile;

		final ReplyKeyboardMarkup menuReplyKeyboard = new ReplyKeyboardMarkup(buttonsList);
		menuReplyKeyboard.resizeKeyboard(true);
		menuReplyKeyboard.oneTimeKeyboard(true);
		return menuReplyKeyboard;
	}

	/**
	 * This method generates a menu with the Settings exposed by this Bot
	 * 
	 * @param userId
	 * @return
	 */
	public ReplyKeyboardMarkup showLanguageMenu(Integer userId) {

		final Locale currentUserLocale = getCurrentUserLocale(userId);

		final KeyboardButton englishLanguage = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getEnglishLanguage(), currentUserLocale));
		final KeyboardButton germanLanguage = new KeyboardButton(
				messageSourceAccessor.getMessage(properties.getGermanLanguage(), currentUserLocale));

		final KeyboardButton[][] buttonsList = new KeyboardButton[1][2];
		buttonsList[0][0] = englishLanguage;
		buttonsList[0][1] = germanLanguage;

		final ReplyKeyboardMarkup menuReplyKeyboard = new ReplyKeyboardMarkup(buttonsList);
		menuReplyKeyboard.resizeKeyboard(true);
		menuReplyKeyboard.oneTimeKeyboard(true);
		return menuReplyKeyboard;
	}

	/**
	 * This method exposes an inline keyboard with the locations searched by the
	 * user, showcases as a [3][3] matrix
	 * 
	 * @param citiesList
	 * @return
	 */
	public InlineKeyboardMarkup citiesListInlineKeyboard(final List<Location> citiesList) {
		final InlineKeyboardButton[][] citiesKeyboardButtons = new InlineKeyboardButton[3][3];
		int cityIndex = 0;
		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {
				if (cityIndex < citiesList.size()) {
					final InlineKeyboardButton contactButton = new InlineKeyboardButton(
							citiesList.get(cityIndex).getName())
									.callbackData(ChatbotConstants.LOC.concat(citiesList.get(cityIndex).getName()))
									.switchInlineQuery(ChatbotConstants.TEST);
					citiesKeyboardButtons[i][j] = contactButton;
					cityIndex++;
				} else {
					final InlineKeyboardButton contactButton = new InlineKeyboardButton(ChatbotConstants.N_A)
							.callbackData(ChatbotConstants.LOC_N_A);
					citiesKeyboardButtons[i][j] = contactButton;
				}
			}
		}

		final InlineKeyboardMarkup contactDetailsKeyboard = new InlineKeyboardMarkup(citiesKeyboardButtons);
		return contactDetailsKeyboard;
	}

	/**
	 * This method checks the user's current language and creates a suitable Locale
	 * object
	 * 
	 * @param userId
	 * @return
	 */
	public Locale getCurrentUserLocale(final Integer userId) {

		final Person currentUser = personRepo.findById(userId);
		Locale currentUserLocale;

		switch (currentUser.getCurrentLanguage()) {
		case "English": {
			currentUserLocale = Locale.ENGLISH;
			break;
		}
		case "Deutsch": {
			currentUserLocale = Locale.GERMANY;
			break;
		}
		default:
			currentUserLocale = Locale.ENGLISH;
		}
		return currentUserLocale;
	}

}