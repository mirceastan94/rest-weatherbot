package com.weather.telegram_chatbot_starter.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;
import com.weather.telegram_chatbot_starter.dao.IMessageDAO;
import com.weather.telegram_chatbot_starter.dao.IPersonDAO;
import com.weather.telegram_chatbot_starter.utils.MenuUtils;

@Service
public class ChangeLanguageButtonAction implements MessageCommandAction<Void> {

	@Autowired
	private IMessageDAO messageDAO;

	@Autowired
	private IPersonDAO personDAO;

	@Autowired
	private MenuUtils menuUtils;

	@Override
	public Void execute(TelegramBot bot, Message message) {

		final Integer chatId = message.from().id();
		final Integer messageId = message.messageId();

		Locale updatedLanguage;

		switch (message.text()) {
		case "English": {
			updatedLanguage = Locale.ENGLISH;
			break;
		}
		case "Deutsch": {
			updatedLanguage = Locale.GERMANY;
			break;
		}
		default:
			updatedLanguage = Locale.ENGLISH;
		}

		LocaleContextHolder.setLocale(updatedLanguage);

		personDAO.insertCurrentLanguage(message.text(), chatId);

		final SendMessage botResponse = new SendMessage(chatId,
				String.format(
						messageDAO.getMessage("languageUpdated", personDAO.getPerson(chatId).getCurrentLanguage())))
								.parseMode(ParseMode.HTML).disableNotification(false).replyToMessageId(messageId)
								.replyMarkup(menuUtils.showMainMenu(chatId));

		bot.execute(botResponse);

		return null;
	}

}