package com.weather.telegram_chatbot_starter.dao;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

import com.pengrad.telegrambot.model.Contact;
import com.weather.telegram_chatbot_starter.model.*;

public interface IPersonDAO {

	public List<Person> getAllPersons();

	public Person getPerson(int userId);

	public void insertPerson(Contact contact);

	public void insertPerson(int userId);

	public void deletePerson(int chatId);

	public String getFavoriteLocationForUser(int userId);

	public String getLastSearchedLocationForUser(int userId);

	public String getNotificationHourForUser(int userId);

	public String getCurrentLanguageForUser(int userId);

	public String getNotificationStatusForUser(int userId);

	public List<Location> getRecentHistoryForUser(int userId);

	@Transactional
	public void insertLocation(String location, int userId);

	@Transactional
	public void insertLastSearchedLocation(String location, int userId);

	@Transactional
	public void insertFavoriteLocation(String location, int userId);

	@Transactional
	public void insertNotificationHour(String hour, int userId);

	@Transactional
	public void insertNotificationStatus(String string, int userId);

	@Transactional
	public void insertCurrentLanguage(String string, int userId);

}
