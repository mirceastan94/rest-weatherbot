package com.weather.telegram_chatbot_starter.dao;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.pengrad.telegrambot.model.Contact;
import com.weather.telegram_chatbot_starter.model.Location;
import com.weather.telegram_chatbot_starter.model.Person;
import com.weather.telegram_chatbot_starter.repo.LocationRepo;
import com.weather.telegram_chatbot_starter.repo.PersonRepo;
import com.weather.telegram_chatbot_starter.utils.ChatbotConstants;

@Repository
public class PersonDAO implements IPersonDAO {

	/** The Constant LOGGER. */
	public static final Logger LOGGER = LogManager.getLogger();

	final private PersonRepo personRepo;

	final private LocationRepo locationRepo;

	public PersonDAO(PersonRepo personRepo, LocationRepo locationRepo) {
		this.personRepo = personRepo;
		this.locationRepo = locationRepo;
	}

	@Override
	@CachePut(value = "persons")
	public List<Person> getAllPersons() {
		return personRepo.findAll();
	}

	@Override
	@CachePut(value = "persons")
	public Person getPerson(int userId) {
		final Person person = personRepo.findById(userId);
		return person;
	}

	@Override
	@CachePut(value = "favoriteLocations")
	public String getFavoriteLocationForUser(int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null && person.getFavoriteCity() != null) {
			return person.getFavoriteCity().getName();
		}
		return null;
	}

	@Override
	@CachePut(value = "lastSearchedLocations")
	public String getLastSearchedLocationForUser(int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null && person.getLastSearchedCity() != null) {
			return person.getLastSearchedCity();
		}
		return null;
	}

	@Override
	@CachePut(value = "notificationsHour")
	public String getNotificationHourForUser(int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null && person.getNotificationHour() != null) {
			return person.getNotificationHour();
		}
		return null;
	}

	@Override
	@CachePut(value = "notificationStatus")
	public String getNotificationStatusForUser(int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null && person.getNotificationStatus() != null) {
			return person.getNotificationStatus();
		}
		return null;
	}

	@Override
	@CachePut(value = "userHistory")
	public List<Location> getRecentHistoryForUser(int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null && !person.getCity().isEmpty()) {
			if (person.getCity().size() > 9) {
				final List<Location> lastCitiesList = person.getCity().subList(person.getCity().size() - 9,
						person.getCity().size());
				return lastCitiesList;
			} else {
				final List<Location> lastCitiesList = person.getCity();
				return lastCitiesList;
			}
		}
		return null;
	}

	@Override
	@CachePut(value = "currentLanguage")
	public String getCurrentLanguageForUser(int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null && person.getCurrentLanguage() != null) {
			return person.getCurrentLanguage();
		}
		return null;
	}

	@Override
	@CacheEvict(value = "persons")
	public void insertPerson(Contact contact) {
		LOGGER.info("Trying to update user information with additional information ...");
		final Person person = new Person();
		person.setId(contact.userId());
		person.setCurrentLanguage(ChatbotConstants.LANGUAGE_ENGLISH_EN);
		person.setPhoneNumber(contact.phoneNumber());
		person.setFirstName(contact.firstName());
		person.setLastName(contact.lastName());
		personRepo.save(person);
		LOGGER.info("Additional user information has been added");
	}

	@Override
	@Transactional
	@CacheEvict(value = "persons")
	public void insertPerson(int chatId) {
		LOGGER.info("Trying to insert a new user...");
		final Person person = new Person();
		person.setId(chatId);
		person.setNotificationStatus(Boolean.FALSE.toString());
		person.setCurrentLanguage(ChatbotConstants.LANGUAGE_ENGLISH_EN);
		personRepo.save(person);
		LOGGER.info("New user has been inserted");
	}

	@Override
	@Transactional
	@CacheEvict(value = "persons")
	public void deletePerson(int chatId) {
		LOGGER.info("Trying to delete user...");
		final Person person = personRepo.findById(chatId);
		personRepo.delete(person);
		LOGGER.info("User has been deleted");
	}

	@Override
	@Transactional
	@CacheEvict(value = "locations")
	public void insertLocation(String inputLocation, int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null) {
			LOGGER.info("Trying to add new location...");
			Location userLocation = locationRepo.findByName(inputLocation);
			if (userLocation != null) {
				LOGGER.info("Location is already in the database with Id: " + userLocation.getId());
				if (!person.getCity().contains(userLocation)) {
					person.getCity().add(userLocation);
					personRepo.save(person);
				}
			} else {
				LOGGER.info("Location will be added in the database...");
				userLocation = new Location();
				userLocation.setName(inputLocation);
				locationRepo.save(userLocation);
				LOGGER.info("Location was saved in the database with Id: " + userLocation.getId());
				person.getCity().add(userLocation);
				personRepo.save(person);
			}
		}
	}

	@Override
	@Transactional
	@CacheEvict(value = "locations")
	public void insertFavoriteLocation(String inputLocation, int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null) {
			LOGGER.info("Trying to add favorite location...");
			Location newLocation = locationRepo.findByName(inputLocation);
			if (newLocation != null) {
				person.setFavoriteCity(newLocation);
				LOGGER.info("Location already exists in the database: " + newLocation.getId());
				personRepo.save(person);
				LOGGER.info("Favorite location has been saved!");
			} else {
				LOGGER.info("Location will be added in the database...");
				newLocation = new Location();
				newLocation.setName(inputLocation);
				locationRepo.save(newLocation);
				LOGGER.info("Location was saved in the database: " + newLocation.getId());
				person.setFavoriteCity(newLocation);
				personRepo.save(person);
			}
		}
	}

	@Override
	@Transactional
	@CacheEvict(value = "locations")
	public void insertLastSearchedLocation(String location, int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null) {
			LOGGER.info("Trying to add last searched location...");
			person.setLastSearchedCity(location);
			personRepo.save(person);
			LOGGER.info("Last searched location column has been updated!");
		}
	}

	@Override
	@Transactional
	@CacheEvict(value = "notifications")
	public void insertNotificationHour(String hour, int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null) {
			LOGGER.info("Trying to add notification hour...");
			person.setNotificationHour(hour);
			personRepo.save(person);
			LOGGER.info("Notification hour has been saved!");
		}
	}

	@Override
	@Transactional
	@CacheEvict(value = "notificationStatus")
	public void insertNotificationStatus(String status, int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null) {
			LOGGER.info("Trying to change the notifications status...");
			person.setNotificationStatus(status);
			personRepo.save(person);
			LOGGER.info("Notifications status has been updated!");
		}
	}

	@Override
	@Transactional
	@CacheEvict(value = "language")
	public void insertCurrentLanguage(String currentLanguage, int userId) {
		final Person person = personRepo.findById(userId);
		if (person != null) {
			LOGGER.info("Trying to change the current language...");
			person.setCurrentLanguage(currentLanguage);
			personRepo.save(person);
			LOGGER.info("Current language has been updated!");

		}
	}
}
