package com.weather.telegram_chatbot_starter.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * This is the entity class for the Person table
 * 
 * @author stan4
 *
 */
@Entity
@Table(name = "PERSON", schema = "public")
public class Person {

	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "PHONE_NUM", unique = true)
	private String phoneNumber;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;

	@ManyToMany(fetch = FetchType.LAZY)
	private List<Location> city = new ArrayList<Location>();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "favorite_city_id")
	private Location favoriteCity;

	@Column(name = "LAST_SEARCHED_CITY")
	private String lastSearchedCity;

	@Column(name = "NOTIFICATION_HOUR")
	private String notificationHour;

	@Column(name = "NOTIFICATIONS_ACTIVATED")
	private String notificationStatus;

	@Column(name = "CURRENT_LANGUAGE")
	private String currentLanguage;

	public Person(String phone) {
		phoneNumber = phone;
	}

	public Person() {

	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Location> getCity() {
		return city;
	}

	public void setCity(List<Location> city) {
		this.city = city;
	}

	public Location getFavoriteCity() {
		return favoriteCity;
	}

	public void setFavoriteCity(Location favoriteCity) {
		this.favoriteCity = favoriteCity;
	}

	public String getLastSearchedCity() {
		return lastSearchedCity;
	}

	public void setLastSearchedCity(String lastSearchedCity) {
		this.lastSearchedCity = lastSearchedCity;
	}

	public String getNotificationHour() {
		return notificationHour;
	}

	public void setNotificationHour(String notificationHour) {
		this.notificationHour = notificationHour;
	}

	public String getNotificationStatus() {
		return notificationStatus;
	}

	public void setNotificationStatus(String notificationStatus) {
		this.notificationStatus = notificationStatus;
	}

	public String getCurrentLanguage() {
		return currentLanguage;
	}

	public void setCurrentLanguage(String currentLanguage) {
		this.currentLanguage = currentLanguage;
	}

}
