package com.weather.telegram_chatbot_starter.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * This class represents the application properties configuration
 * 
 * @author stan4
 *
 */
@Configuration
@PropertySource("classpath:application.properties")
public class Properties {
	/** The hibernate auto. */
	@Value("${hibernate.hbm2ddl.auto}")
	private String hibernateAuto;

	/** The hibernate show sql. */
	@Value("${hibernate.show_sql}")
	private String hibernateShowSql;

	/** The hibernate dialect. */
	@Value("${hibernate.dialect}")
	private String hibernateDialect;

	/** The jdbc driver class name. */
	@Value("${datasource.driverClassName}")
	private String jdbcDriverClassName;

	/** The jdbc url. */
	@Value("${datasource.url}")
	private String jdbcUrl;

	/** The jdbc username. */
	@Value("${datasource.username}")
	private String jdbcUsername;

	/** The jdbc password. */
	@Value("${datasource.password}")
	private String jdbcPassword;

	/** The bot API key. */
	@Value("${key.bot_api}")
	private String botApiKey;

	/** The weather API key. */
	@Value("${key.weather_api}")
	private String weatherApiKey;

	/** The geocoding API key. */
	@Value("${key.geocoding_api}")
	private String geocodingApiKey;

	/** The days forecast search count. */
	@Value("${forecast.daily_hours}")
	private int dailyHours;

	/** The next search hours interval. */
	@Value("${forecast.next_search_hours_interval}")
	private int nextSearchHoursInterval;

	/** The next three days forecast entries list. */
	@Value("${forecast.next_three_days_entries_count}")
	private int nextThreeDaysEntriesCount;

	/** The daily information gather count. */
	@Value("${forecast.daily_gather_count}")
	private int weatherGatherCount;

	/** The current location weather information button */
	@Value("${menu.current_location_weather_information}")
	private String currentLocationWeatherInformation;

	/** The another location weather information button */
	@Value("${menu.another_location_weather_information}")
	private String anotherLocationWeatherInformation;

	/** The search list history button */
	@Value("${menu.search_list_history}")
	private String searchListHistory;

	/** The show forecast button */
	@Value("${menu.show_forecast}")
	private String showForecast;

	/** The back to menu button */
	@Value("${menu.go_back_to_menu}")
	private String goBackToMenu;

	/** The share contact details button */
	@Value("${menu.share_contact_details}")
	private String shareContactDetails;

	/** The deny button */
	@Value("${menu.deny}")
	private String deny;

	/** The application settings button */
	@Value("${menu.application_settings}")
	private String applicationSettings;

	/** The update location information button */
	@Value("${menu.update_location_information}")
	private String updateLocationInformation;

	/** The change language button */
	@Value("${menu.change_language}")
	private String changeLanguage;

	/** The language english button */
	@Value("${menu.language_english}")
	private String englishLanguage;

	/** The language german button */
	@Value("${menu.language_german}")
	private String germanLanguage;

	/** The delete profile button */
	@Value("${menu.delete_profile}")
	private String deleteProfile;

	/** The menu notifications off button */
	@Value("${menu.notifications_off}")
	private String notificationsOff;

	/** The menu notifications on button */
	@Value("${menu.notifications_on}")
	private String notificationsOn;

	/** The weather temperature info */
	@Value("${weather.temperature}")
	private String weatherTemperature;

	/** The weather description info */
	@Value("${weather.description}")
	private String weatherDescription;

	/** The weather rainfall info */
	@Value("${weather.rainfall}")
	private String weatherRainfall;

	/** The weather pressure info */
	@Value("${weather.pressure}")
	private String weatherPressure;

	/** The weather humidity info */
	@Value("${weather.humidity}")
	private String weatherHumidity;

	/** The weather date info */
	@Value("${forecast.date}")
	private String forecastDate;

	/** The weather averageTemperature info */
	@Value("${forecast.averageTemperature}")
	private String forecastAverageTemperature;

	/** The weather minimumTemperature info */
	@Value("${forecast.minimumTemperature}")
	private String forecastMinimumTemperature;

	/** The weather maximumTemperature info */
	@Value("${forecast.maximumTemperature}")
	private String forecastMaximumTemperature;

	/** The forecast description info */
	@Value("${forecast.description}")
	private String forecastDescription;

	/** The forecast pressure info */
	@Value("${forecast.pressure}")
	private String forecastPressure;

	/** The forecast humidity info */
	@Value("${forecast.humidity}")
	private String forecastHumidity;

	/** The forecast rainfall info */
	@Value("${forecast.rainfall}")
	private String forecastRainfall;

	public String getBotApiKey() {
		return botApiKey;
	}

	public void setBotApiKey(String botApiKey) {
		this.botApiKey = botApiKey;
	}

	public String getWeatherApiKey() {
		return weatherApiKey;
	}

	public void setWeatherApiKey(String weatherApiKey) {
		this.weatherApiKey = weatherApiKey;
	}

	public String getGeocodingApiKey() {
		return geocodingApiKey;
	}

	public void setGeocodingApiKey(String geocodingApiKey) {
		this.geocodingApiKey = geocodingApiKey;
	}

	public String getHibernateAuto() {
		return hibernateAuto;
	}

	public void setHibernateAuto(String hibernateAuto) {
		this.hibernateAuto = hibernateAuto;
	}

	public String getHibernateShowSql() {
		return hibernateShowSql;
	}

	public void setHibernateShowSql(String hibernateShowSql) {
		this.hibernateShowSql = hibernateShowSql;
	}

	public String getHibernateDialect() {
		return hibernateDialect;
	}

	public void setHibernateDialect(String hibernateDialect) {
		this.hibernateDialect = hibernateDialect;
	}

	public String getJdbcDriverClassName() {
		return jdbcDriverClassName;
	}

	public void setJdbcDriverClassName(String jdbcDriverClassName) {
		this.jdbcDriverClassName = jdbcDriverClassName;
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getJdbcUsername() {
		return jdbcUsername;
	}

	public void setJdbcUsername(String jdbcUsername) {
		this.jdbcUsername = jdbcUsername;
	}

	public String getJdbcPassword() {
		return jdbcPassword;
	}

	public void setJdbcPassword(String jdbcPassword) {
		this.jdbcPassword = jdbcPassword;
	}

	public int getDailyHours() {
		return dailyHours;
	}

	public void setDailyHours(int dailyHours) {
		this.dailyHours = dailyHours;
	}

	public int getNextSearchHoursInterval() {
		return nextSearchHoursInterval;
	}

	public void setNextSearchHoursInterval(int nextSearchHoursInterval) {
		this.nextSearchHoursInterval = nextSearchHoursInterval;
	}

	public int getNextThreeDaysEntriesCount() {
		return nextThreeDaysEntriesCount;
	}

	public void setNextThreeDaysEntriesCount(int nextThreeDaysEntriesCount) {
		this.nextThreeDaysEntriesCount = nextThreeDaysEntriesCount;
	}

	public int getWeatherGatherCount() {
		return weatherGatherCount;
	}

	public void setWeatherGatherCount(int weatherGatherCount) {
		this.weatherGatherCount = weatherGatherCount;
	}

	public String getCurrentLocationWeatherInformation() {
		return currentLocationWeatherInformation;
	}

	public void setCurrentLocationWeatherInformation(String currentLocationWeatherInformation) {
		this.currentLocationWeatherInformation = currentLocationWeatherInformation;
	}

	public String getAnotherLocationWeatherInformation() {
		return anotherLocationWeatherInformation;
	}

	public void setAnotherLocationWeatherInformation(String anotherLocationWeatherInformation) {
		this.anotherLocationWeatherInformation = anotherLocationWeatherInformation;
	}

	public String getSearchListHistory() {
		return searchListHistory;
	}

	public void setSearchListHistory(String searchListHistory) {
		this.searchListHistory = searchListHistory;
	}

	public String getShowForecast() {
		return showForecast;
	}

	public void setShowForecast(String showForecast) {
		this.showForecast = showForecast;
	}

	public String getGoBackToMenu() {
		return goBackToMenu;
	}

	public void setGoBackToMenu(String goBackToMenu) {
		this.goBackToMenu = goBackToMenu;
	}

	public String getShareContactDetails() {
		return shareContactDetails;
	}

	public void setShareContactDetails(String shareContactDetails) {
		this.shareContactDetails = shareContactDetails;
	}

	public String getDeny() {
		return deny;
	}

	public void setDeny(String deny) {
		this.deny = deny;
	}

	public String getUpdateLocationInformation() {
		return updateLocationInformation;
	}

	public void setUpdateLocationInformation(String updateLocationInformation) {
		this.updateLocationInformation = updateLocationInformation;
	}

	public String getChangeLanguage() {
		return changeLanguage;
	}

	public void setChangeLanguage(String changeLanguage) {
		this.changeLanguage = changeLanguage;
	}

	public String getDeleteProfile() {
		return deleteProfile;
	}

	public void setDeleteProfile(String deleteProfile) {
		this.deleteProfile = deleteProfile;
	}

	public String getApplicationSettings() {
		return applicationSettings;
	}

	public void setApplicationSettings(String applicationSettings) {
		this.applicationSettings = applicationSettings;
	}

	public String getEnglishLanguage() {
		return englishLanguage;
	}

	public void setEnglishLanguage(String englishLanguage) {
		this.englishLanguage = englishLanguage;
	}

	public String getGermanLanguage() {
		return germanLanguage;
	}

	public void setGermanLanguage(String germanLanguage) {
		this.germanLanguage = germanLanguage;
	}

	public String getNotificationsOff() {
		return notificationsOff;
	}

	public void setNotificationsOff(String notificationsOff) {
		this.notificationsOff = notificationsOff;
	}

	public String getNotificationsOn() {
		return notificationsOn;
	}

	public void setNotificationsOn(String notificationsOn) {
		this.notificationsOn = notificationsOn;
	}

	public String getWeatherTemperature() {
		return weatherTemperature;
	}

	public void setWeatherTemperature(String weatherTemperature) {
		this.weatherTemperature = weatherTemperature;
	}

	public String getWeatherDescription() {
		return weatherDescription;
	}

	public void setWeatherDescription(String weatherDescription) {
		this.weatherDescription = weatherDescription;
	}

	public String getWeatherRainfall() {
		return weatherRainfall;
	}

	public void setWeatherRainfall(String weatherRainfall) {
		this.weatherRainfall = weatherRainfall;
	}

	public String getWeatherPressure() {
		return weatherPressure;
	}

	public void setWeatherPressure(String weatherPressure) {
		this.weatherPressure = weatherPressure;
	}

	public String getWeatherHumidity() {
		return weatherHumidity;
	}

	public void setWeatherHumidity(String weatherHumidity) {
		this.weatherHumidity = weatherHumidity;
	}

	public String getForecastDate() {
		return forecastDate;
	}

	public void setForecastDate(String forecastDate) {
		this.forecastDate = forecastDate;
	}

	public String getForecastAverageTemperature() {
		return forecastAverageTemperature;
	}

	public void setForecastAverageTemperature(String forecastAverageTemperature) {
		this.forecastAverageTemperature = forecastAverageTemperature;
	}

	public String getForecastMinimumTemperature() {
		return forecastMinimumTemperature;
	}

	public void setForecastMinimumTemperature(String forecastMinimumTemperature) {
		this.forecastMinimumTemperature = forecastMinimumTemperature;
	}

	public String getForecastMaximumTemperature() {
		return forecastMaximumTemperature;
	}

	public void setForecastMaximumTemperature(String forecastMaximumTemperature) {
		this.forecastMaximumTemperature = forecastMaximumTemperature;
	}

	public String getForecastDescription() {
		return forecastDescription;
	}

	public void setForecastDescription(String forecastDescription) {
		this.forecastDescription = forecastDescription;
	}

	public String getForecastPressure() {
		return forecastPressure;
	}

	public void setForecastPressure(String forecastPressure) {
		this.forecastPressure = forecastPressure;
	}

	public String getForecastHumidity() {
		return forecastHumidity;
	}

	public void setForecastHumidity(String forecastHumidity) {
		this.forecastHumidity = forecastHumidity;
	}

	public String getForecastRainfall() {
		return forecastRainfall;
	}

	public void setForecastRainfall(String forecastRainfall) {
		this.forecastRainfall = forecastRainfall;
	}

}
