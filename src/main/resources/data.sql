-- English --

INSERT INTO advice (id,language,condition,emoji,message) VALUES ('1','English','Clear',':sunny:','It is time for a picnic. A clear blue sky is waiting for you.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('2','English','Few clouds',':partly_sunny:','The sun is hiding from you momentarily, yet life can still be enjoyable.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('3','English','Clouds',':cloud:','Ergh, it could have been worse, just some clouds at the moment.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('4','English','Rain',':umbrella:','Do not forget the shower gel, as you are going to take a bath today.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('5','English','Thunderstorm',':zap:','Thunderstruck! Oh, sorry, too much AC/DC ruined my mind. In a positive way, that is. Nevermind, I can report a thunderstom as we ... speak') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('6','English','Mist',':foggy:','Please drive carefully, the mist is here!') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('7','English','Snow',':snowflake:','The snow is snowing, enjoy the flakes!') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('8','English','Haze',':foggy:','Haze ... haze everywhere, watch out!') ON CONFLICT DO NOTHING;

INSERT INTO message (id,language,meaning,message) VALUES ('1','English','askLocation','Please choose your location first, by using the format "/loc yourLocation"') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('2','English','applicationSettings','My settings are present below.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('3','English','mainMenu','Below you can find my main menu.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('4','English','deny','Alright, got it. Now, what would you wish to know?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('5','English','noLocationReceived','You must enter a location to receive the desired information!') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('6','English','processingException','Your location could not be processed. 
Please try again and make sure you enter an existing one!') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('7','English','userSearchedLocationsList','Your recent search list history can be found below. Please click on a specific button to gather the current weather information for that location.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('8','English','noLocationSearched','You did not search any location until now. 
It is never too late, though.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('9','English','savedContactDetails','Your contact details have been saved internally (%s).
Now please enter your favorite local hour (between 0 and 23) to receive weather notifications from a location that you will choose momentarily, using the format "/hour yourHour".') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('10','English','notificationStatusChanged','Your notifications status has been changed. My main menu can be found below.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('11','English','savedNotificationHour','Your desired local notification hour has been saved internally (%s).
Now please enter your favorite location for which you want your notifications to be received, using the format "/fav yourLocation"') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('12','English','invalidNotificationHourValue','Your desired notification hour does not have a range between 0-23 (%s). 
Please enter a valid hour using the format "/hour desiredHour", between 0 and 23') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('13','English','invalidNotificationHourFormat','Your desired notification hour is not a numeric value (%s). 
Please enter a valid hour using the format "/hour desiredHour", between 0 and 23') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('14','English','conversionException','The location could not be parsed due to a conversion error, please try again later!') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('15','English','startRegisteredUser','Welcome back, %s. What would you wish to check right now?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('16','English','startNewUser','Hello. I am your friendly WeatherBOT. 
To receive daily notifications of your favorite location weather, please share your contact details by pressing the "Share contact details" button which can be found below.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('17','English','weatherNoSave','Below you have the current weather information for %s :
%s 

Would you like to check the forecast for this location too?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('18','English','weatherSavedLocation','Your location has been saved internally (%s), for either notification purposes or if you want to check the personal search history later.

Below you have the current weather information: 
%s

Would you like to check the forecast for this location too?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('19','English','forecast','Below is the forecast for %s: 

%s') ON CONFLICT DO NOTHING;

INSERT INTO message (id,language,meaning,message) VALUES ('20','English','changeLanguage','Choose one of the languages below.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('21','English','languageUpdated','Your language has been updated. Below you can find my main menu.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('22','English','deletedProfile','Your account has been deleted. You can now forget about WeatherBOT or start using it again from scratch. To do so, type "/start"') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('23','English','unknown','Uhm ... I do not quite understand what you are telling me.
Please use one (or more) of my functionalities from the menu below.') ON CONFLICT DO NOTHING;


-- Deutsch --

INSERT INTO advice (id,language,condition,emoji,message) VALUES ('9','Deutsch','Clear',':sunny:','Es ist Zeit f�r ein Picknick. Ein klarer blauer Himmel wartet auf dich.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('10','Deutsch','Few clouds',':partly_sunny:','Die Sonne versteckt sich momentan vor dir, doch das Leben kann immer noch angenehm sein.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('11','Deutsch','Clouds',':cloud:','Vergiss, es h�tte schlimmer kommen k�nnen, nur ein paar Wolken im Moment.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('12','Deutsch','Rain',':umbrella:','Vergessen Sie nicht das Duschgel, denn Sie werden heute ein Bad nehmen.') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('13','Deutsch','Thunderstorm',':zap:','Thunderstruck! Oh, Entschuldigung, zu viel AC / DC hat meine Meinung ruiniert. Im positiven Sinne. Niemals, ich kann ein Gewitter melden, wenn wir ... sprechen') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('14','Deutsch','Mist',':foggy:','Bitte fahren Sie vorsichtig, der Nebel ist da!') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('15','Deutsch','Snow',':snowflake:','Der Schnee schneit, genie�en Sie die Flocken!') ON CONFLICT DO NOTHING;
INSERT INTO advice (id,language,condition,emoji,message) VALUES ('16','Deutsch','Haze',':foggy:','Dunst ... Dunst �berall, pass auf!') ON CONFLICT DO NOTHING;

INSERT INTO message (id,language,meaning,message) VALUES ('24','Deutsch','askLocation','Bitte w�hle zuerst deinen Standort, indem du das Format "/loc deinStandort" verwendest') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('25','Deutsch','applicationSettings','Meine Einstellungen sind unten aufgef�hrt.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('26','Deutsch','mainMenu','Unten finden Sie mein Hauptmen�.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('27','Deutsch','deny','Okay, verstanden. Nun, was m�chtest du wissen?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('28','Deutsch','noLocationReceived','Sie m�ssen einen Ort eingeben, um die gew�nschten Informationen zu erhalten!') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('29','Deutsch','processingException','Ihr Standort konnte nicht verarbeitet werden. 
Bitte versuchen Sie es erneut und vergewissern Sie sich, dass Sie ein bestehendes eingeben!') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('30','Deutsch','userSearchedLocationsList','Ihre aktuelle Suchlistenhistorie finden Sie unten. Bitte klicken Sie auf eine bestimmte Schaltfl�che, um die aktuellen Wetterinformationen f�r diesen Standort zu erhalten.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('31','Deutsch','noLocationSearched','Du hast bisher keinen Ort gesucht. 
Es ist jedoch nie zu sp�t.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('32','Deutsch','savedContactDetails','Ihre Kontaktdaten wurden intern gespeichert (%s).
Geben Sie nun Ihre bevorzugte lokale Stunde (zwischen 0 und 23) ein, um Wetterbenachrichtigungen von einem Ort zu erhalten, den Sie momentan zu ausw�hlen werden, und zwar im Format "/hour yourHour".') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('33','Deutsch','notificationStatusChanged','Ihr Benachrichtigungsstatus wurde ge�ndert. Mein Hauptmen� finden Sie unten.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('34','Deutsch','savedNotificationHour','Ihre gew�nschte lokale Benachrichtigungsstunde wurde intern gespeichert (%s).
Geben Sie nun Ihren bevorzugten Ort, f�r den Sie Ihre Benachrichtigungen erhalten m�chten, im Format "/fav yourLocation" ein.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('35','Deutsch','invalidNotificationHourValue','Ihre gew�nschte Benachrichtigungsstunde hat keinen Bereich zwischen 0-23 (%s). 
Bitte geben Sie eine g�ltige Stunde im Format "/hour gew�nschteStunde", zwischen 0 und 23') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('36','Deutsch','invalidNotificationHourFormat','Ihre gew�nschte Benachrichtigungsstunde ist kein numerischer Wert (%s). 
Bitte geben Sie eine g�ltige Stunde im Format "/hour gew�nschteStunde", zwischen 0 und 23') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('37','Deutsch','conversionException','Der Standort konnte aufgrund eines Konvertierungsfehlers nicht analysiert werden. Bitte versuchen Sie es sp�ter erneut!') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('38','Deutsch','startRegisteredUser','Willkommen zur�ck, %s. Was m�chten Sie jetzt �berpr�fen?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('39','Deutsch','startNewUser','Hallo. Ich bin dein freundlicher WeatherBOT. 
Um t�gliche Benachrichtigungen zu Ihrem bevorzugten Wetterort zu erhalten, teilen Sie bitte Ihre Kontaktdaten mit, indem Sie auf die Schaltfl�che "Kontaktinformationen teilen" klicken, die unten zu finden ist.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('40','Deutsch','weatherNoSave','Im Folgenden finden Sie die aktuellen Wetterinformationen f�r %s :
%s 

M�chten Sie auch die Prognose f�r diesen Standort pr�fen?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('41','Deutsch','weatherSavedLocation','Ihr Standort wurde intern gespeichert (%s), entweder f�r Benachrichtigungszwecke oder wenn Sie den pers�nlichen Suchverlauf sp�ter �berpr�fen m�chten.

Im Folgenden haben Sie die aktuellen Wetterinformationen: 
%s

M�chten Sie auch die Prognose f�r diesen Standort pr�fen?') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('42','Deutsch','forecast','Unten ist die Prognose f�r %s: 

%s') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('43','Deutsch','changeLanguage','W�hlen Sie eine der folgenden Sprachen.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('44','Deutsch','languageUpdated','Ihre Sprache wurde aktualisiert. Unten finden Sie mein Hauptmen�.') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('45','Deutsch','deletedProfile','Dein Account wurde gel�scht. Sie k�nnen jetzt WeatherBOT vergessen oder es von Grund auf neu verwenden. Um dies zu tun, tippen Sie "/start"') ON CONFLICT DO NOTHING;
INSERT INTO message (id,language,meaning,message) VALUES ('46','Deutsch','unknown','Uhm ... Ich verstehe nicht ganz, was du mir erz�hlst.
Bitte verwenden Sie eine oder mehrere meiner Funktionen aus dem folgenden Men�.') ON CONFLICT DO NOTHING;